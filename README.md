# artwork_future_weapons
![ScreenShot](https://github.com/alaskalinuxuser/artwork_future_weapons/blob/master/icon.png)

This is a small collection of futuristic buildings, vehicles, weapons, objects, etc., that can be used for any purpose. They are licensed under the CC0, and are readily available for use in games, as icons, or whatever the use/case may be. Enjoy!

-Alaskalinuxuser
